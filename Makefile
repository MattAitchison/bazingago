GOROOT=/usr/local/go
GOPATH=$(shell pwd)

run:
	GOROOT=${GOROOT} GOPATH=${GOPATH} revel run bazinga
build: get
	GOROOT=${GOROOT} GOPATH=${GOPATH} revel build bazinga
revel:
	GOROOT=${GOROOT} GOPATH=${GOPATH} go get github.com/revel/revel
get:
	GOROOT=${GOROOT} GOPATH=${GOPATH} go get ...

.PHONY: run get build revel