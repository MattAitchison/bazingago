package models
//import (
//	"fmt"
//	"github.com/coopernurse/gorp"
//	"github.com/revel/revel"
//	"regexp"
//	"time"
//)

type Series struct {
	Id string `gorethink:"id,omitempty"`
	Title   string `gorethink:"title"`
	Overview  string `gorethink:"overview"`
}
