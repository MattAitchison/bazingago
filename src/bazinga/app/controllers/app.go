package controllers

import (
	"github.com/revel/revel"
	"bazinga/app/services"

)

type App struct {
	*revel.Controller
	services.RethinkController
}

func (c App) Index() revel.Result {

	return c.Render()
}
