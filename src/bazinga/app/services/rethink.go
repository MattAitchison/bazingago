package services

import (
	r "github.com/dancannon/gorethink"
	"github.com/revel/revel"
	"time"
)

var (
	rdb_session *r.Session
	Host string
	Port string
	Database string
//	Retries string
//	MaxIdle string
//	IdleTimeout string
)

func RethinkInit() {
	// Read configuration.
	var found bool
	var err error

	if Host, found = revel.Config.String("rethink.address"); !found {
		// Default to 'localhost'
		Host = "localhost"
	}
	if Port, found = revel.Config.String("rethink.port"); !found {
		Port = "28015"
	}
	if Database, found = revel.Config.String("rethink.db"); !found {
		panic("Config rethink.db must be set")
	}
//	if Retries, found = revel.Config.String("rethikn.retries"); !found {
//		Retries = "5"
//	}
//	if MaxIdle, found = revel.Config.String("rethikn.maxIdle"); !found {
//		MaxIdle = "10"
//	}
//	if IdleTimeout, found = revel.Config.String("rethikn.idleTimeout"); !found {
//		IdleTimeout = time.Second * 10
//	}


	rdb_session, err = r.Connect(map[string]interface{}{
		"address": Host + ":" + Port,
		"database": Database,
		"retries": 5,
		"maxIdle": 10,
		"idleTimeout": time.Second * 10,
})
	if err != nil {
		panic(err)
	}

	if revel.Config.BoolDefault("rethink.resetDb", false) == true {
		setupRethinkDB(true, true)
	}
}


func ControllerInit() {
	revel.InterceptMethod((*RethinkController).Begin, revel.BEFORE)
}

type RethinkController struct {
	*revel.Controller
	Db *r.Session
}

// Connect to mgo if we haven't already and return a copy/new/clone of the session and database connection
func (c *RethinkController) Begin() *r.Session {
	c.Db = rdb_session
	return nil
}
