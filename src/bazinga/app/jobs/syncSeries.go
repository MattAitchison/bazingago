package jobs

import (
	"fmt"
	"github.com/revel/revel"
	"github.com/revel/revel/modules/jobs/app/jobs"

)

// Periodically count the bookings in the database.
type BookingCounter struct{}

func (c BookingCounter) Run() {

	fmt.Printf("There are YAY")
}

func init() {
	revel.OnAppStart(func() {
		jobs.Now(BookingCounter{})
	})
}
